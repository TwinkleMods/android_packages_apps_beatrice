package com.cluster.beatrice;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class TabTutorialActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tab_tutorial);
		
		Button okay = (Button) findViewById(R.id.okay);
	    okay.setOnClickListener(new OnClickListener()
	    {
	        public void onClick(View v)
	        {
	            finish();
	        }
	    });
	}

}
